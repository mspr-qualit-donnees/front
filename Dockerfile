FROM nginx:latest

RUN rm -f /etc/nginx/sites-enabled/default.conf /etc/nginx/conf.d/default.conf

COPY docker-conf/nginx.conf /etc/nginx/
COPY docker-conf/site.conf /etc/nginx/conf.d/
COPY --chown=www-data:www-data datavinci/dist /srv

WORKDIR /srv