import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseurl = ' http://api.data-vinci-code.tk/';

  constructor(private http: HttpClient) {
  }


  public getArtworksImports() {
    return this.http.get(this.baseurl + 'artworks/imports-per-year');
  }

  public getArtworksImportsMocked() {
    return[
      {
        value: 7,
        name: '2003'
      },
      {
        value: 20,
        name: '2004'
      },
      {
        value: 80,
        name: '2005'
      },
      {
        value: 270,
        name: '2006'
      },
      {
        value: 800,
        name: '2008'
      },
      {
        value: 1200,
        name: '2010'
      },
      {
        value: 2000,
        name: '2015'
      }
    ];
  }

  public getArtworksWomenMocked() {
    return [
      {
        value: 0,
        name: '1500'
      },
      {
        value: 2,
        name: '1525'
      },
      {
        value: 2,
        name: '1550'
      },
      {
        value: 8,
        name: '1575'
      },
      {
        value: 8,
        name: '1600'
      },
      {
        value: 8,
        name: '1625'
      },
      {
        value: 10,
        name: '1650'
      },
      {
        value: 10,
        name: '1675'
      },
      {
        value: 10,
        name: '1700'
      },
      {
        value: 15,
        name: '1725'
      },
      {
        value: 15,
        name: '1750'
      },
      {
        value: 15,
        name: '1775'
      },
      {
        value: 15,
        name: '1800'
      },
      {
        value: 23,
        name: '1825'
      },
      {
        value: 23,
        name: '1850'
      },
      {
        value: 30,
        name: '1875'
      },
      {
        value: 52,
        name: '1900'
      }
    ];
  }

  public getArtworksSylvainMocked() {
    return {
      area: '25598',
      in: {
        sylvain: '3'
      },
      eq: [],
      out: {
        football_field: '2789',
        aircraft_carrier: '6574'
      }
    };
  }

  public getNbArtworks() {
    return this.http.get(this.baseurl + 'artworks/count');
  }

  public getArtworksSylvain() {
    return this.http.get(this.baseurl + 'artworks/total-area');
  }

  public getArtworksMaterials(){
    return this.http.get(this.baseurl + 'artworks/quantities-per-domain');
  }

  public getArtworksMaterialsMocked() {
    return {
      peinture: {
        area: 19856,
        area_unit: 'cm2',
        quantity_used: 1.9856,
        product_name: 'Peinture',
        unit: 'L'
      },
      sculpture: {
        volume: 3871,
        volume_unit: 'cm3',
        quantity_used: 10.4517,
        product_name: 'Marbre',
        unit: 'kg'
      }
    };
  }
}
