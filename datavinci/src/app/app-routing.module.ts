import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GraphComponent} from './components/graph/graph.component';
import {SylvainComponent} from './components/sylvain/sylvain.component';
import {HomeComponent} from './components/home/home.component';
import {MaterialsComponent} from './components/materials/materials.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'graph/:id', component: GraphComponent },
  { path: 'superficie', component: SylvainComponent },
  { path: 'materials', component: MaterialsComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
