import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  nbArt = 0;
  nbArtFromApi = 0;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getNbArtworks().subscribe((res) =>  {
      // @ts-ignore
      this.nbArtFromApi = parseInt(res, 10);
      setInterval(() => {
        if (this.nbArt >= this.nbArtFromApi) {
          clearInterval();
          this.nbArt = this.nbArtFromApi;
        } else {
          this.nbArt += 100;
        }
      }, 15);
    });

  }

}
