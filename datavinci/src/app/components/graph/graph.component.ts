import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../services/api/api.service';
import {random, TinyColor} from '@ctrl/tinycolor';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit, OnChanges {

  dataset: any [];

  view: any[] = [900, 500];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#004500', '#00a900', '#405f6c']
  };
  title = 'Chargement ...';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  public loadData() {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    if ( id === '1') {
      this.apiService.getArtworksImports().subscribe((res) => {
        this.generateGraph(
          'Nombre d\'oeuvres importées par an',
          'Année',
          'Nombre d\'oeuvres importées',
          res
        );
      });

    } else if ( id === '2') {
      this.generateGraph(
        'Nombre d\'oeuvres faites par des femmes par an',
        'Année',
        'Nombre d\'oeuvres',
        this.apiService.getArtworksWomenMocked()
      );
    }
  }

  private generateGraph(title, xAxisLabel, yAxisLabel, dataset) {
    this.title = title;
    this.xAxisLabel = xAxisLabel;
    this.yAxisLabel = yAxisLabel;
    this.dataset = dataset;
    this.colorScheme.domain = ['#1976D2'];
  }

  public colorShadeGenerator(nbItem) {
    const beginColor = new TinyColor('green');
    const colors = beginColor.analogous(nbItem);
    return colors.map(t => t.toHexString());
  }

  ngOnInit() {
    this.loadData();
  }


  ngOnChanges(changes: SimpleChanges): void {
    this.loadData();
  }

}
