import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SylvainComponent } from './sylvain.component';

describe('SylvainComponent', () => {
  let component: SylvainComponent;
  let fixture: ComponentFixture<SylvainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SylvainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SylvainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
