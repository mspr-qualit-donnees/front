import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-sylvain',
  templateUrl: './sylvain.component.html',
  styleUrls: ['./sylvain.component.scss']
})
export class SylvainComponent implements OnInit {
  displayAllSylvains = false;

  sylvainHeight = 0;
  sylvainWidth = 0;
  surface = 0;
  artworksSurface = 0;
  artworksSurfaceTotal = 0;
  numberOfSylvains = 0;

  constructor(private apiService: ApiService) {
  }

  getSylvainMockedData() {
    const sylvainMockedData = this.apiService.getArtworksSylvainMocked();
    this.artworksSurfaceTotal = parseInt(sylvainMockedData.area, 10);
    this.numberOfSylvains = parseInt(sylvainMockedData.in.sylvain, 10);
  }


  getSylvainData() {
    this.apiService.getArtworksSylvain().subscribe((res) => {
      // @ts-ignore
      this.numberOfSylvains = res.in.sylvain;
      // @ts-ignore
      this.artworksSurfaceTotal = res.area;
      this.counter();

    });
  }

  counter() {

    // This block will be executed 100 times.
    setInterval(() => {
      if (this.sylvainHeight === 170) {
        clearInterval();
      } else {
        this.sylvainHeight += 2;
      }
    }, 15);
    setInterval(() => {
      if (this.sylvainWidth === 50) {
        clearInterval();
      } else {
        this.sylvainWidth++;
      }
    }, 15);
    setInterval(() => {
      if (this.surface >= 0.85) {
        clearInterval();
        this.surface = 0.85;
      } else {
        this.surface += 0.01;
      }

    }, 15);
    setInterval(() => {
      if (this.artworksSurface >= this.artworksSurfaceTotal) {
        clearInterval();
        this.artworksSurface = this.artworksSurfaceTotal;
      } else {
        this.artworksSurface += 1000;
      }
    }, 15);
  } // End
  ngOnInit() {
    this.getSylvainData();

  }


}
