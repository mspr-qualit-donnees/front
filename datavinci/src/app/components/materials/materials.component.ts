import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.scss']
})
export class MaterialsComponent implements OnInit {

  canvasWidth = 500;
  x = 0;
  y = 0;
  paintLiters = 0;
  mockedMaxPaintLiters = 0;
  stoneKg = 0;
  mockedMaxStoneKg = 0;
  showLegend = false;

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {

    this.apiService.getArtworksMaterials().subscribe((res) => {
       // @ts-ignore
      this.mockedMaxPaintLiters = parseInt(res.peinture.quantity_used, 10) / 100;
       // @ts-ignore
     // this.mockedMaxStoneKg = res.sculpture.quantity_used;
      this.start(0);
      const paintInterval = setInterval(() => {
        if (this.paintLiters >= this.mockedMaxPaintLiters) {
          clearInterval(paintInterval); this.paintLiters = this.mockedMaxPaintLiters; this.showLegend = true;
          // this.startStoneInterval();
        } else { this.paintLiters++; }
      }, 1);
    });



  }

  startStoneInterval() {
    const stoneInterval = setInterval(() => {
      if (this.stoneKg >= this.mockedMaxStoneKg) {
        clearInterval(stoneInterval); this.stoneKg = this.mockedMaxStoneKg;
      } else { this.stoneKg++; }
    }, 0.5);
  }
  start(counter) {
    const c = document.getElementById('myCanvas');
    // @ts-ignore
    const ctx = c.getContext('2d');
    const size = 10;
    if (counter < this.mockedMaxPaintLiters + this.mockedMaxStoneKg) {
      setTimeout(() => {
        let color = 'blue';
        if (counter <= this.mockedMaxPaintLiters ) {
          color = 'red';
        }
        counter++;
        this.drawSquare(ctx, size, color);
        this.start(counter);
      }, 1);
    }
  }

  private drawSquare(ctx, size: number, color) {
    ctx.beginPath();
    ctx.rect(this.x, this.y, size, size);
    ctx.fillStyle = color;
    ctx.fill();
    ctx.strokeStyle = 'white';
    ctx.stroke();
    this.x += size;
    if (this.x === this.canvasWidth) {
      this.x = 0;
      this.y += size;
    }
  }
}
